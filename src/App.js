import React, { Component } from 'react';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import Checkout from "./components/checkout/Checkout";
import ShoppingCart from "./components/shopping-cart/ShoppingCart";
import NavBar from "./components/appbar/NavBar";
import LoginForm from "./components/login/LoginForm";
import ProductThumb from './components/productThumb/ProductThumb';
import ShowRoom from './components/showRoom/ShowRoom';
import ProductInfo from './components/productInfo/ProductInfo';
import Footer from './components/footer/Footer';
import Carousel from './components/header/Carousel';
import Home from './components/pages/Home';

import {
  Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import history from "./components/history";

import "./App.css";

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}

const theme = createMuiTheme({
  palette:{
    primary: { main: '#4CAF50', contrastText: '#ffffff' },
    secondary: { main: '#00796B', contrastText: '#ffffff' }
  },
});

class App extends Component {
  state = {  }

  render() {
    
    return (
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <NavBar/>
          <Carousel/>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/productos">
              <About />
            </Route>
            <Route path="/users">
              <Users />
            </Route>
            <Route exact path="/checkout" component={Checkout}/>
          </Switch>
          <Footer/>
        </Router>
      </ThemeProvider>
     );
  }
}
 
export default App;
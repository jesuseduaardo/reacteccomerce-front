export const dataItems = [
    { 
      id: 1,
      productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
      productName:"Control de Playstation",
      productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
      price:10,
      tax:5,
      qty:1,
      stock:1,
      discountPercent:30
    },
    { 
      id: 2,
      productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
      productName:"Control de Playstation",
      productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
      price:10,
      tax:5,
      qty:1,
      stock:2,
      discountPercent:0
    },
    { 
        id: 3,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:1,
        discountPercent:0
      },
      { 
        id: 4,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:1,
        discountPercent:0,
        deliveryFee: 0
      },
      { 
        id: 5,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:1,
        discountPercent:0
      },

  ];

  export const checkout = [
    { 
        id: 1,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:2,
        discountPercent:30,
        deliveryFee: 0
      },
      { 
        id: 2,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:2,
        deliveryFee: 3
      },
      { 
        id: 3,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:2,
        deliveryFee: 3
      },
      { 
        id: 4,
        productImg:"http://www.digitaless.com/wp-content/uploads/2018/04/Joystick-Sony-PS4-original-negro_2.jpg",
        productName:"Control de Playstation",
        productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
        price:10,
        tax:5,
        qty:1,
        stock:2,
        deliveryFee: 3
      }
  ]
export const montoTotal = price => {
    const discountPercent = typeof price.discountPercent!=="undefined" && price.discountPercent!=="" 
                            ? price.discountPercent : 0;
    const priceWithTax = price.price + price.tax;
    const discountAmount =  (priceWithTax * discountPercent) / 100;
    const totalWithDiscount = priceWithTax - discountAmount;
    const values = {
        'discountPercent':discountPercent,
        'priceWithTax':priceWithTax,
        'discountAmount':discountAmount,
        'totalWithDiscount':totalWithDiscount
    }
    return values;
}  

export const summaryTotal=dataItems=>{
    if(dataItems!==null && dataItems.length > 0){
        let subtotal = 0;
        let impuestos = 0;
        let descuentoMonto = 0;
        let total = 0;
        let envio=0;

        for (let i = 0; i < dataItems.length; i++) {
          let item = dataItems[i];
          let discountPercent = typeof item.discountPercent!=="undefined" && item.discountPercent!=="" 
          ? item.discountPercent : 0;
          if(discountPercent > 0){

              let discountAmount    = (item.price * discountPercent) / 100;
              let taxdiscountAmount = (item.tax * discountPercent) / 100;
            //   let priceWithDiscount = item.price - discountAmount;
            //   let taxWithDiscount   = item.price - taxdiscountAmount;
            //   subtotal += (priceWithDiscount * item.qty);
            //   impuestos += (taxWithDiscount * item.qty);
              descuentoMonto += (discountAmount+taxdiscountAmount)* item.qty;
            }
            envio += item.deliveryFee;
            subtotal += (item.price * item.qty);
            impuestos += (item.tax * item.qty);
        }
        total = (subtotal + impuestos - descuentoMonto);
        return { total, subtotal, impuestos, descuentoMonto, envio }
    }
    return 0;
}
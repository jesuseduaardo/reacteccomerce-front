import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import {withRouter} from 'react-router';
import Slider from 'react-animated-slider';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import 'react-animated-slider/build/horizontal.css';
import './carousel.css';
import './animations.css';


const styles = theme => ({
    sliderTitle:{
        color:"#FFFF"
    },
    sliderParagraph:{
        color:"#FFFF"
    }
});

const content = [
    { title: 'First item', description: 'Lorem ipsum', button:'Read more', image:'https://cdn.pixabay.com/photo/2019/11/29/14/18/gent-4661419_960_720.jpg'},
    { title: 'Second item', description: 'Lorem ipsum', button:'Read more', image:'https://cdn.pixabay.com/photo/2017/07/28/00/57/bank-2547356_960_720.jpg'}
  ];

class Carousel extends Component {
    state = {  }

    banner=()=>{
        return(
            <div className="banner"></div>
        )
    }

    carousel =()=> {
        const {classes} = this.props;
        return(
            <Slider className="slider-wrapper">
                    {content.map((item, index) => (
                        <div
                        key={index}
                        className="slider-content"
                        style={{ background: `url('${item.image}') no-repeat center center` }}
                        >
                            <div className="inner">
                                <Typography variant="h2" className={classes.sliderTitle}>
                                    Heading
                                </Typography>
                                <Typography paragraph className={classes.sliderParagraph} gutterBottom>
                                    Lorem ipsum
                                </Typography>
                                <Button variant="contained" color="primary">
                                    Action
                                </Button>
                            </div>
                        </div>
                    ))}
            </Slider>
        );
    }

    render() { 
        const handle = this.props.location.pathname;
        const match = handle.match(/^.([a-z]+)/i);

        return (
            <React.Fragment>
                { typeof match !== "undefined" && match!=null && Array.isArray(match) ? 
                        match[1] === "checkout" ? this.banner() : this.carousel() : this.carousel()
                } 
            </React.Fragment>
         );
    }
}
 
export default withRouter(withStyles(styles)(Carousel));
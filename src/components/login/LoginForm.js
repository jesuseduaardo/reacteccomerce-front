import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({

});

class LoginForm extends Component {
    
    state = { 
        open: false
     }


    handleClickOpen = () => {
        this.setState({
            ...this.state,
            open:true
        })
    };

    handleClose = () => {
        this.setState({
            ...this.state,
            open:false
        })
    };

    render() { 
        return ( 
            <div>
                <Button style={{color:"#fff"}} onClick={this.handleClickOpen}>
                    Mi Cuenta
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Accede a tu cuenta</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                        Ingresa y disfruta de las promociones y descuentos exclusivos para usuarios del sitio
                    </DialogContentText>

                    <TextField 
                        margin="dense"
                        id="email"
                        label="Correo electronico"
                        type="email"
                        fullWidth
                    />
                    <TextField
                        margin="dense"
                        id="pass"
                        label="Contraseña"
                        type="password"
                        fullWidth
                    />
                    </DialogContent>
                    <DialogActions style={{padding:"8 24px"}}>
                    <Button onClick={this.handleClose} variant="contained" color="primary">
                        Acceder
                    </Button>
                    </DialogActions>
                    <Divider />
                    <DialogContent>
                        <DialogContentText style={{textAlign:"center", lineHeight:"5px", marginTop:"20px"}}>
                            ¿Aún no tienes tu cuenta?
                            <Button onClick={this.handleClose} color="primary">
                                Registrate ahora
                            </Button>
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            </div>
         );
    }
}
 
export default withStyles(styles)(LoginForm);
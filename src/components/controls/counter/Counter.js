import React, { Component } from 'react';
import "./counter.css";
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

class Counter extends Component {

    state = { 
        'count':1,
        'stock':1
     }

    componentDidMount(){
         this.setState({
             ...this.state,
             'stock': this.props.stock
            })
    }
        
    componentDidUpdate(prevProps, prevState){
        if(this.props !== prevProps){
            this.setState({
                ...this.state,
                'stock': this.props.stock
            })
        }
    }

     addValue = () =>{
         if(this.state.count < this.state.stock){
             const newCount = this.state.count + 1;
             this.setState({
                 'count' : newCount
             }, ()=>{
                this.props.updateQty(newCount);
             })
         }
     }

     lessValue = () =>{
         if(this.state.count > 1){
            const newCount = this.state.count - 1;
            this.setState({
                count : newCount
            }, ()=>{
                this.props.updateQty(newCount);
             })
         }
    }

    render() { 

        return ( 
            <React.Fragment>
                <div className="contenedor">
                    <div className="boton-izquierdo">
                        <IconButton onClick={this.lessValue}>
                                <KeyboardArrowLeft />
                        </IconButton>
                    </div>
                    <input  type="text" className="inputContador" 
                            value={this.state.count} 
                            onChange={(event)=>{
                                const val = parseInt(event.target.value);
                                this.setState({
                                    count: isNaN(val) ? 0 : parseInt(event.target.value)
                                })
                            }} />
                    <IconButton onClick={this.addValue}>
                            <KeyboardArrowRight />
                    </IconButton>
                </div>
            </React.Fragment>
            
         );
    }
}
 
export default Counter;
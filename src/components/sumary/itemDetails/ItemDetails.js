import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Clear from '@material-ui/icons/Clear';
import Counter from '../../controls/counter/Counter'
import Hidden from '@material-ui/core/Hidden';
import {montoTotal} from '../../utils/utils';
import "./itemDetails.css";

const styles = theme => ({
  card: {
    
  },
  actions: {
    verticalAlign: 'middle',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
    verticalAlign: 'middle',
    margin: 'auto',
    textAlign: 'center'
  },
  pos: {
    marginBottom: 12,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 75,
  },
});

class ItemDetails extends React.Component {
  state = { 
    product:{}
   }

  componentDidMount(){
    if(this.props.data!==""){
      this.setState({
        product:{...this.props.data}
      })
    }
  }

  componentDidUpdate(prevProps){
    if(this.props!==prevProps){
      if(this.props.data!==""){
        this.setState({
          product:{...this.props.data}
        })
      }
    }
  }

  updateQty =(count)=> {
    //setProduct({...product, [product.qty]:count})
    this.props.updateQty(this.state.product.id, count);
  }

  showPrice(price){

    const discountPercent = typeof price.discountPercent!=="undefined" && price.discountPercent!=="" 
                            ? price.discountPercent : 0;
    const priceWithTax = price.price + price.tax;
    const discountAmount =  (priceWithTax * discountPercent) / 100;
    const totalWithDiscount = priceWithTax - discountAmount;
    return(
    <React.Fragment>
        
    </React.Fragment>
    );
}
  render() { 

    const {classes} = this.props;
    const product = this.state.product;
    const values = montoTotal(product);
    return ( 
      <Card className="card">
        <CardHeader
            action={
            <IconButton aria-label="delete" onClick={ ()=> this.props.deleteItem(product.id) }>
                <Clear />
            </IconButton>
            }
            title={ 
              <Typography>
                {product.productName} 
              </Typography>
            }
      />
      <CardContent style={{padding:"2px 10px 10px 0px"}}>
        <Grid container spacing={0}>
            <Grid container item xs={6} md={2}>
              <div style={{textAlign:"center"}}>
                <Avatar 
                    style={{width:'40%', margin:'auto'}}
                    variant="square" 
                    src={product.productImg} />
                
                {product.discountPercent > 0 ? 
                  <Typography variant="body1" className="old-price" >
                    ${product.qty * values.priceWithTax}
                  </Typography>
                : "" }
                <Typography gutterBottom 
                    variant="h5" 
                    component="h2" 
                    display="inline" 
                    align="left">
                    ${ product.qty * values.totalWithDiscount}
                </Typography>
                {product.discountPercent > 0 ?
                    <Typography 
                        variant="body2" 
                        className="promo-color discount-percent" 
                        component="p"
                        >
                         {values.discountPercent}
                    </Typography>
                :""}
              </div>
            </Grid>
            <Hidden xsDown>
                <Grid container item md={8} className={classes.actions} direction="row" justify="flex-start" alignItems="center">
                    {product.productDescription}
                </Grid>
            </Hidden>
            <Grid container item xs={5} md={2} className={classes.actions} direction="row" justify="center" alignItems="center">
                
                <Counter stock={product.stock} updateQty={this.updateQty} />
            </Grid>
        </Grid>
      </CardContent>
    </Card>

     );
  }
}
 
export default withStyles(styles)(ItemDetails);
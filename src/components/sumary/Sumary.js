import React from 'react';
import { withStyles } from '@material-ui/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import ItemDetails from './itemDetails/ItemDetails';
import Divider from '@material-ui/core/Divider';  
import "./sumary.css";

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor:"#fff",
  },
  inline: {
    display: 'inline',
  },
});

class Sumary extends React.Component {
  state = { 
    productList:[]
   }

   componentDidMount(){
    
      this.setState({
        ...this.state,
        productList : this.props.dataItems
      })
    
  }

   componentDidUpdate(prevProps){
     if(this.props !== prevProps){
       this.setState({
         ...this.state,
         productList : this.props.dataItems
       })
     }
   }

  handleRemoveItem = id => {
    const productList = this.state.productList.filter(item => item.id !== id)
    this.props.updateSumary(productList);
  }

  updateQty = (id, count) => {
    const array = this.state.productList;
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      if(element.id === id){
        element.qty = count;
      }
    }
    this.props.updateSumary(array);
  }

  getTotal(data){
    const discountPercent = typeof data.discountPercent !== "undefined" ? data.discountPercent : 0;
    const discount = (data.price * discountPercent) / 100; 
    const tax = (data.tax - (data.tax * discountPercent) / 100) * data.qty;
    const subtotal = (data.price - discount) * data.qty;
    const total =  subtotal + tax;
    return total;
  }

  itemList(){
    const { classes } = this.props;
    const info = this.state.productList;
    let total = 0;

    const list = Object.keys(info).map((item, i) => (
                  <ListItem key={i} total = {total += this.getTotal(info[i])}>
                      <ItemDetails data={info[i]} key={i} deleteItem={this.handleRemoveItem} updateQty={this.updateQty} />
                  </ListItem>
              ))

    return (
      <React.Fragment>
        <Typography variant="h6" gutterBottom>Resumen de Compra</Typography>
        <List className={classes.root}>
            { typeof info !== "undefined" ? list : "" }
        </List>
        <Divider />
        <Typography variant="h6" className="total" gutterBottom>Total ${total}</Typography>
      </React.Fragment>
    )
  }

  render() { 
    
    const info = this.state.productList;
    
    return ( 
        typeof info !== "undefined" ? 
          this.itemList() : 
          <Typography variant="h6" gutterBottom>No hay elementos en el carrito</Typography>
     );
  }
}
 
export default  withStyles(styles)(Sumary);
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ImageGallery from 'react-image-gallery';
import ProductCost from './productDescription/ProductCost';
import ProductDescription from './productDescription/ProductDescription';
import "react-image-gallery/styles/css/image-gallery.css";

const styles = theme => ({
   
  });

  const dataItems = { 
      id: 1,
      productImg:[
        {
            original: 'https://picsum.photos/id/1018/1000/600/',
            thumbnail: 'https://picsum.photos/id/1018/250/150/',
        },
        {
            original: 'https://picsum.photos/id/1015/1000/600/',
            thumbnail: 'https://picsum.photos/id/1015/250/150/',
        },
        {
            original: 'https://picsum.photos/id/1019/1000/600/',
            thumbnail: 'https://picsum.photos/id/1019/250/150/',
        },
      ],
      productName:"Control de Playstation",
      productDescription:"This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.",
      price:10,
      tax:5,
      qty:1,
      stock:2,
      discountPercent:30,
      deliveryFee: 100
    }

class ProductInfo extends Component {
    state = {  }
    render() { 
        return ( 
            <Grid container spacing={5}>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12}>
                        <ImageGallery items={dataItems.productImg} />
                    </Grid>
                </Grid>
                <Grid item xs={12} md={5}>
                    <ProductCost data={dataItems}/>
                </Grid>
                <Grid item xs={12} md={7}>
                    <ProductDescription data={dataItems}/>
                </Grid>
            </Grid>
         );
    }
}
 
export default withStyles(styles)(ProductInfo);
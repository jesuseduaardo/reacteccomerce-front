import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Button from '@material-ui/core/Button';
import {montoTotal} from '../../utils/utils';

const styles = theme => ({
   cantidad:{
       display:"flex",
       alignItems:"end",
       paddingTop:10
   },
   envioGratis:{
       display:"flex",
       alignItems:"center",
       paddingTop:10
   },
   actionButtons:{
       margin:"10px auto",
       padding:"10px 5px", 
       width:"90%",
       display:"block"
   }
});

class ProductCost extends Component {
    state = { 
        cantidad:1
     }

    productStock=(stock)=>{
        const numbers = []
        for (let i = 1; i <= stock; i++) {
            numbers.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
        } 
        return numbers;
    }

    handleChange = event => {
        this.setState({
            cantidad: event.target.value
        });
      };

    render() { 
        const {classes, data} = this.props;
        const values = montoTotal(data);
        const cantidad = this.productStock(data.stock);
        return ( 
            <div>
                <Typography variant="h6" style={{paddingBottom:12   }} gutterBottom>
                    {data.productName}
                </Typography>
                <Typography variant="body1" className="old-price" >
                        ${values.priceWithTax}
                </Typography>
                <div style={{display:"flex", alignItems:"center"}}>
                    <Typography variant="h4" display="inline">
                        ${values.totalWithDiscount}
                    </Typography>
                    <Typography 
                        variant="body1" 
                        display="inline" 
                        className="promo-color discount-percent" 
                        style={{paddingLeft:16}} 
                        gutterBottom>
                        {values.discountPercent}
                    </Typography>
                </div>
                {
                    data.stock > 0 ? 
                    <Typography 
                        variant="body2" 
                        gutterBottom>
                        Stock disponible
                    </Typography>
                    :""
                }
                {
                    data.deliveryFee === 0 ?
                    <div className={classes.envioGratis}>
                        <LocalShipping className="promo-color" style={{paddingRight:6}} />
                        <Typography 
                            className="promo-color" 
                            variant="body2" 
                            display="inline"
                            gutterBottom>
                                Envio gratis
                        </Typography>
                    </div>
                    :
                    <div className={classes.envioGratis}>
                        <LocalShipping style={{paddingRight:6}} />
                        <Typography 
                            variant="body2" 
                            display="inline"
                            gutterBottom>
                                Costo de envio ${data.deliveryFee}
                        </Typography>
                    </div>
                }
                <div className={classes.cantidad}>
                    <Typography 
                        variant="body1" 
                        display="inline" 
                        style={{padding:"5px 10px 5px 0"}} 
                        gutterBottom>
                        Cantidad:
                    </Typography>
                    <TextField
                        id="outlined-select-currency"
                        select
                        value={this.state.cantidad}
                        onChange={(e)=>this.handleChange(e)}
                        >
                        {cantidad}
                    </TextField>
                </div>
                <div style={{paddingTop:25}}>
                    <Button className={classes.actionButtons} variant="contained" color="primary">
                        Comprar ahora
                    </Button>

                    <Button size="small" className={classes.actionButtons} color="primary">
                        Agregar al carrito
                    </Button>
                </div>
            </div>
         );
    }
}
 
export default withStyles(styles)(ProductCost);
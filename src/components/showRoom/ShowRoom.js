import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ProductThumb from '../productThumb/ProductThumb';
import "./showroom.css";

import { dataItems } from '../../data';

const styles = theme => ({
    root: {
        flexGrow: 1,
      },
  });

class ShowRoom extends Component {
    state = { 
        productList:[]
     }

    componentDidMount(){
        this.setState({
          ...this.state,
          productList : dataItems
        })
      
    }
  
     componentDidUpdate(prevProps){
       if(this.props !== prevProps){
         this.setState({
           ...this.state,
           productList : dataItems
         })
       }
     }


    render() { 
        const {classes} = this.props;
        const info = this.state.productList;
        return ( 
            <div className={classes.root}>
                <Grid container>
                    { typeof this.state.productList !== "undefined" ?
                        Object.keys(info).map((item, i) => (
                            <ProductThumb key={i} data={info[i]}/>
                        ))
                        :""
                    }
                </Grid>
            </div>
         );
    }
}
 
export default withStyles(styles)(ShowRoom);
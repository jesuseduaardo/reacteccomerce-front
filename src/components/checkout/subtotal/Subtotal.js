import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import {summaryTotal} from '../../utils/utils';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Subtotal extends React.Component {
  state = { 
    dataItems:[],
    cost : {
      impuestos: 0,
      subtotal: 0,
      total: 0,
      descuentoPorc: 0,
      descuentoMonto: 0
    },
   }

   componentDidMount(){
    const totales = summaryTotal(this.props.dataItems);
    if(totales !==0){
      this.setState({
        ...this.state,
        cost:{
          ...this.state.cost,
          subtotal:totales.subtotal,
          impuestos:totales.impuestos,
          total:totales.total,
          descuentoMonto:totales.descuentoMonto,
          envio:totales.envio
        }
      })
    }
   }

   componentDidUpdate(prevProps){
    if(this.props !== prevProps){
      const totales = summaryTotal(this.props.dataItems);
      if(totales !==0){
        this.setState({
          ...this.state,
          cost:{
            ...this.state.cost,
            subtotal:totales.subtotal,
            impuestos:totales.impuestos,
            total:totales.total,
            descuentoMonto:totales.descuentoMonto,
            envio:totales.envio
          }
        })
      }
    }
  }

  render() { 
    const { classes } = this.props;
    return ( 
      <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
            <List className={classes.root}>
                <ListItem>
                    <ListItemText primary="Impuestos" />
                    ${typeof this.state.cost.impuestos !== "undefined" ? this.state.cost.impuestos.toFixed(2) : 0}
                </ListItem>
                
                <ListItem>
                    <ListItemText primary="Descuentos" />
                    - ${typeof this.state.cost.descuentoMonto !== "undefined" ? this.state.cost.descuentoMonto.toFixed(2) : 0}
                </ListItem>
                <Divider  component="li" />
                <ListItem>
                    <ListItemText primary="Subtotal" />
                    ${typeof this.state.cost.subtotal !== "undefined" ? this.state.cost.subtotal.toFixed(2) : 0}
                </ListItem>
                <Divider  component="li" />
                <ListItem>
                    <ListItemText primary="Total productos" />
                    ${typeof this.state.cost.total !== "undefined" ? this.state.cost.total.toFixed(2) : 0 }
                </ListItem>
                { typeof this.state.cost.total !== "undefined" && typeof this.state.cost.envio !== "undefined" ?
                  <React.Fragment>
                    <Divider  component="li" />
                    <ListItem>
                        <ListItemText primary="Envio" />
                        ${this.state.cost.envio.toFixed(2)}
                    </ListItem>
                    <Divider  component="li" />
                    <ListItem>
                        <ListItemText primary="Total con envio" />
                        ${this.state.cost.total + this.state.cost.envio}
                    </ListItem>
                  </React.Fragment>
                  :""}
            </List>
        </Grid>
      </Grid>
    </div>

     );
  }
}
 
export default withStyles(styles)(Subtotal);
import React from 'react';
import { withStyles } from '@material-ui/styles';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Paymethod from './paymethod/Paymethod';
import Subtotal from './subtotal/Subtotal';
import Sumary from '../sumary/Sumary';
import AddressForm from './addressform/AddressForm';
import PaymethodSelect from './paymethodSelect/PaymethodSelect';

import "./checkout.css";

import {checkout} from "../../data";

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

class Checkout extends React.Component {
  state = { 
    sumary: [],
    activeStep:0,
   }

  getSteps() {
    return ['Direccion de envio', 'Metodo de pago', 'Confirma orden'];
  }

  componentDidMount(){
     this.setState({
        ...this.state,
        sumary:checkout  
     })
   }

  handleNext = () => {
    this.setState({
      ...this.state,
      activeStep:this.state.activeStep + 1
    });
  };

  handleBack = () => {
    this.setState({
      ...this.state,
      activeStep:this.state.activeStep - 1
    });
  };

  handleReset = () => {
    this.setState({
      ...this.state,
      activeStep:0
    });
  };

  getStepContent(step) {
    switch (step) {
      case 0:
        return (<AddressForm/>);
      case 1:
        return (<PaymethodSelect/>);
      case 2:
        return this.checkoutTotal(this.state);
      default:
        return 'Unknown step';
    }
  }

  updateSumary = (oldSumary) => {
    this.setState({
      ...this.state,
      sumary:oldSumary
    });
  }

  checkoutTotal=(state)=>{
    const { classes } = this.props;
    return(
      <Card className={classes.card}>
        <CardContent>
          <Sumary dataItems={state.sumary} updateSumary={this.updateSumary} />
          <Subtotal dataItems={state.sumary} />
        </CardContent>
      </Card>
    )
  }

  render() { 
    const { classes } = this.props;
    const steps = this.getSteps();
    return ( 
      <React.Fragment>
        <Stepper activeStep={this.state.activeStep}>
          {steps.map((label, index) => {
            const stepProps = {};
            const labelProps = {};
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
          {this.state.activeStep === steps.length ? (
            <div className="stepperContent">
              <Typography className={classes.instructions}>
                All steps completed - you&apos;re finished
              </Typography>
              <Button onClick={()=>this.handleReset()} className={classes.button}>
                Reset
              </Button>
            </div>
          ) : (
            <div className="stepperContent">
              {this.getStepContent(this.state.activeStep)}
              <div className="stepperActions">
                <Button disabled={this.state.activeStep === 0} onClick={()=>this.handleBack()} className={classes.button}>
                  Back
                </Button>
                
                  {this.state.activeStep === steps.length - 1 ? <Paymethod/> : 
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={()=>this.handleNext()}
                    className={classes.button}
                  >Next</Button>}
                
              </div>
            </div>
        )}
    </React.Fragment>
     );
  }
}
 
export default withStyles(styles)(Checkout);
import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import "./addressForm.css";

class AddressForm extends Component {
    state = { 
        pickupinstore:false
     }
    render() { 
        return ( 
            <React.Fragment>
                <Typography variant="h6" gutterBottom>
                    Direccion de envio
                </Typography>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <FormControlLabel
                            control={<Checkbox color="secondary" name="pickupinstore" value="yes" />}
                            label="Lo retiro en Tienda"
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="nombre"
                        name="nombre"
                        label="Nombre"
                        fullWidth
                        autoComplete="fnombre"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="apellido"
                        name="apellido"
                        label="Apellido"
                        fullWidth
                        autoComplete="lapellido"
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        required
                        id="direccion1"
                        name="direccion1"
                        label="Direccion 1"
                        fullWidth
                        autoComplete="billing address-line1"
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        id="direccion2"
                        name="direccion2"
                        label="Direccion 2"
                        fullWidth
                        autoComplete="billing address-line2"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="ciudad"
                        name="ciudad"
                        label="Ciudad"
                        fullWidth
                        autoComplete="billing address-level2"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField id="estado" name="estado" label="Estado/Provincia/Region" fullWidth />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="zip"
                        name="zip"
                        label="Codigo Postal"
                        fullWidth
                        autoComplete="billing postal-code"
                    />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="pais"
                        name="pais"
                        label="Pais"
                        fullWidth
                        autoComplete="billing country"
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <FormControlLabel
                        control={<Checkbox color="secondary" name="saveAddress" value="yes" />}
                        label="Usar esta misma direccion para la facturacion"
                    />
                    </Grid>
                </Grid>
            </React.Fragment>
         );
    }
}
 
export default AddressForm;
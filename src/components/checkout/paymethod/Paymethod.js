import React from 'react';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ThumbUp from '@material-ui/icons/ThumbUp';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    width: '100%',
    color: "#fff"
  },
}));

export default function IconLabelButtons() {
  const classes = useStyles();
  
  const [paymentMethod, setPaymentMethod] = React.useState(null);

  return (
    <div>
        {paymentMethod === null ?
        
          <Button
            variant="contained"
            size="large"
            className={classes.button} 
            color="primary"
          > 
            Confirmar Orden
          </Button>
      : <React.Fragment>
            <List className={classes.root}>
                <ListItem>
                    <Button
                        variant="contained"
                        size="large"
                        className={classes.button}
                        startIcon={<ThumbUp />} 
                        color="primary"
                    >
                        Metodo de Pago 1
                    </Button>
                </ListItem>
                <ListItem>
                <Button
                        variant="contained"
                        size="large"
                        className={classes.button}
                        startIcon={<ThumbUp />} 
                        color="primary"
                    >
                        Metodo de Pago 2
                    </Button>
                </ListItem>
            </List>
      </React.Fragment> }
    </div>
  );
}

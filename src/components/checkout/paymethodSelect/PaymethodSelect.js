import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import "./paymentfont.css";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 220,
        backgroundColor: theme.palette.background.paper,
        margin:"auto"
      },
  });

class PaymethodSelect extends Component {
    state = { 
        selectedIndex:""
     }

     handleListItemClick = (event, index) => {
        this.setState({
            selectedIndex: index
        });
      };
    render() { 
        const {classes} = this.props;
        return ( 

            <div className={classes.root}>
                <List component="nav" aria-label="main mailbox folders">
                    <ListItem
                        button
                        selected={this.state.selectedIndex === 0}
                        onClick={event => this.handleListItemClick(event, 0)}
                    >
                        <ListItemIcon>
                            <i className="pf pf-mercado-pago-sign"></i>
                        </ListItemIcon>
                        <ListItemText primary="MercadoPago" />
                    </ListItem>
                    <ListItem
                        button
                        selected={this.state.selectedIndex === 1}
                        onClick={event => this.handleListItemClick(event, 1)}
                    >
                        <ListItemIcon>
                            <i className="pf pf-paypal-alt"></i>
                        </ListItemIcon>
                        <ListItemText primary="PayPal" />
                    </ListItem>
                    <ListItem
                        button
                        selected={this.state.selectedIndex === 3}
                        onClick={event => this.handleListItemClick(event, 3)}
                    >
                        <ListItemIcon>
                            <i className="pf pf-bitcoin-sign"></i>
                        </ListItemIcon>
                        <ListItemText primary="Bitcoin" />
                    </ListItem>
                    <ListItem
                        button
                        selected={this.state.selectedIndex === 2}
                        onClick={event => this.handleListItemClick(event, 2)}
                    >
                        <ListItemIcon>
                            <i className="pf pf-cash"></i>
                        </ListItemIcon>
                        <ListItemText primary="Efectivo" />
                    </ListItem>
                    <Divider />
                    <ListItem
                        button
                        selected={this.state.selectedIndex === 4}
                        onClick={event => this.handleListItemClick(event, 4)}
                        >
                        <ListItemText primary="Acordar con el vendedor" />
                    </ListItem>
                </List>
            </div>
            
         );
    }
}
 
export default withStyles(styles)(PaymethodSelect);
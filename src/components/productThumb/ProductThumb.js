import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Popover from '@material-ui/core/Popover';
import {isMobile} from 'react-device-detect';
import {montoTotal} from '../utils/utils';
import "./product-thumb.css";

const styles = theme => ({
    card: {
        maxWidth: "100%",
      },
      media: {
        minHeight: 250,
        backgroundSize:"contain"
      },
      actionButtons:{
          width:"100%"
      },
      envio:{
        position: "absolute",
        bottom: "-11px",
        right: "26px",
        border: "1px solid",
        borderRadius: "30px",
        padding: "5px",
        zIndex:"9",
        color:"#00a650",
        backgroundColor:"#fff"
      },
      popover: {
        pointerEvents: 'none',
      },
      paper: {
        padding: theme.spacing(1),
      },
  });

class ProductThumb extends Component {
    state = { 
        show: false,
        popoverOpen:false,
        anchorEl:null
     }

    showDescription(show){
        this.setState({
            show:show
        })
    }

    handlePopoverOpen = event => {
        this.setState({
            ...this.state,
            anchorEl:event.currentTarget,
            popoverOpen:true
        })
    }
    
    handlePopoverClose = () => {
        this.setState({
            ...this.state,
            anchorEl:null,
            popoverOpen:false
        })
    }

    showDeliveryFee(){
        const {classes} =  this.props;
        return(
             this.props.data.deliveryFee === 0 ?
            <React.Fragment>
                <LocalShipping 
                    className={classes.envio} 
                    aria-owns={this.popoverOpen ? 'mouse-over-popover' : undefined} 
                    onMouseEnter={(e)=>this.handlePopoverOpen(e)}
                    onMouseLeave={()=>this.handlePopoverClose()} />
                <Popover
                    id="mouse-over-popover"
                    className={classes.popover}
                    classes={{ paper: classes.paper, }}
                    open={this.state.popoverOpen}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    onClose={()=>this.handlePopoverClose}
                    disableRestoreFocus
                >
                    <Typography variant="caption">Envio gratis</Typography>
                </Popover>
            </React.Fragment>
            : ""
        )
    }

    render() { 

        const {classes} = this.props;
        const values = montoTotal(this.props.data);
        if(isMobile && !this.state.show){
            this.setState({
                ...this.state,
                show:true
            })
        }
        return ( 
            <Grid item xs={12} md={3} className="thumbox">
                <Card className={classes.card} 
                    
                    onMouseEnter={ isMobile ? "" : ()=>this.showDescription(true) } 
                    onMouseLeave={ isMobile ? "" : ()=>this.showDescription(false) }
                    
                    >
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image={this.props.data.productImg}
                            title={this.props.data.productName}
                        />
                         {this.props.data.stock == 1 ? 
                                <Typography variant="body2" 
                                    style={{
                                            color:"red", 
                                            paddingLeft:14, 
                                            position:"absolute",
                                            bottom: 0
                                            }} 
                                    component="p">
                                   Ultimo
                                </Typography>
                                :""
                            }
                            {this.showDeliveryFee()}
                    </CardActionArea>
                   
                    {this.props.data.discountPercent > 0 ? 
                        <Collapse in={this.state.show}>
                            <CardContent style={{padding:"8px 16px 0px"}}>
                            <Typography variant="body1" className="old-price" >
                                ${values.priceWithTax}
                            </Typography>
                            </CardContent>
                        </Collapse>
                        : "" }
                        <CardContent style={{padding:"6px 16px 4px"}}>
                            <Grid container>
                                <Grid item xs={6}>
                                    <Typography gutterBottom 
                                        variant="h5" 
                                        component="h2" 
                                        display="inline" 
                                        align="left">
                                        ${values.totalWithDiscount}
                                    </Typography>
                                </Grid>
                                <Grid item xs={6} style={{display: "flex", alignItems: "center"}}>
                                {this.props.data.discountPercent > 0 ?
                                    <Typography 
                                        variant="body1" 
                                        display="inline" 
                                        className="promo-color discount-percent" 
                                        style={{paddingLeft:16}} 
                                        gutterBottom>
                                        {values.discountPercent}
                                    </Typography>
                                :""}
                                </Grid>
                            </Grid>
                        </CardContent>

                    <Collapse in={this.state.show}>
                        <CardContent style={{paddingTop:0, paddingBottom:3}}>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {this.props.data.productName}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Grid container spacing={0}>
                                <Grid item xs={6} md={12}>
                                    <Button className={classes.actionButtons} variant="contained" color="primary">
                                        Comprar ahora
                                    </Button>
                                </Grid>
                                <Grid item xs={6} md={12}>
                                    <Button size="small" className={classes.actionButtons} color="primary">
                                        Agregar al carrito
                                    </Button>
                                </Grid>
                            </Grid>
                        </CardActions>
                    </Collapse>
                </Card>
            </Grid>
            
         );
    }
}

export default withStyles(styles)(ProductThumb);

ProductThumb.propTypes = {
    data: PropTypes.object
};
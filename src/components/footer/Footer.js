import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import YouTubeIcon from '@material-ui/icons/YouTube';
import Link from '@material-ui/core/Link';
import "./footer.css";

const styles = theme => ({
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
  });

class Footer extends Component {
    state = {  }

    y2k(number){ 
        return (number < 1000) ? number + 1900 : number; 
    }

    render() { 
        const {classes} = this.props;
        const today = new Date();
        const year = this.y2k(today.getYear());
        return (
            <div className="footer">
                <Grid container>
                    <Grid item xs={12} md={5} style={{ padding:"0 20px  30px"}}>
                        <Typography variant="h6">Conectate con nosotros!</Typography>
                        <Typography variant="body2">
                            Siguenos en nuestras redes, subscribete y mantente
                            <br/>informado de nuestras promociones y ofertas.
                        </Typography>
                        <div className="subscribe-box">
                            <form className={classes.root} noValidate autoComplete="off">
                                <TextField 
                                    id="suscribe"
                                    label="Email"
                                    type="text"
                                    autoComplete="email"
                                    variant="outlined" 
                                    className="subscribe-txt"
                                    />
                                <Button variant="contained" size="large" color="primary" className="subscribe-button">
                                        Suscribirme
                                </Button>
                            </form>
                        </div>
                        <div className="subscribe-social">
                            <IconButton aria-label="facebook">
                                <FacebookIcon/>
                            </IconButton>
                            <IconButton aria-label="twitter">
                                <TwitterIcon/>
                            </IconButton>
                            <IconButton aria-label="twitter">
                                <InstagramIcon/>
                            </IconButton>
                            <IconButton aria-label="twitter">
                                <WhatsAppIcon/>
                            </IconButton>
                            <IconButton aria-label="twitter">
                                <LinkedInIcon/>
                            </IconButton>
                            <IconButton aria-label="twitter">
                                <YouTubeIcon/>
                            </IconButton>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={4} style={{ padding:"0 20px 30px"}}>
                        <Typography variant="h6">Enlaces de interes</Typography>
                        <ul className="enlaces-interes">
                            <li>
                                <Link
                                    component="button"
                                    variant="body2"
                                    onClick={() => {
                                        console.info("I'm a button.");
                                    }}
                                    >
                                    Nosotros
                                </Link>
                            </li>
                            <li>
                                <Link
                                    component="button"
                                    variant="body2"
                                    onClick={() => {
                                        console.info("I'm a button.");
                                    }}
                                    >
                                    Contactanos
                                </Link>
                            </li>
                            <li>
                                <Link
                                    component="button"
                                    variant="body2"
                                    onClick={() => {
                                        console.info("I'm a button.");
                                    }}
                                    >
                                    Condiciones de servicio
                                </Link>
                            </li>
                        </ul>
                    </Grid>
                    <Grid item xs={12} md={3} style={{ padding:"0 20px 30px"}}>

                    </Grid>
                    <Grid item xs={12} style={{ padding:"0 20px 30px"}}>

                    </Grid>
                    <Grid item xs={12} style={{ padding:"0 20px 30px", display:"flex", justifyContent:"space-around" }}>
                        <Link
                            component="button"
                            variant="body2"
                            onClick={() => {
                                console.info("I'm a button.");
                            }}>
                              Politica de privacidad
                        </Link>
                        <Link
                            component="button"
                            variant="body2"
                            onClick={() => {
                                console.info("I'm a button.");
                            }}>
                              Sitemap
                        </Link>
                        <Typography variant="body2">
                            {'© Copyright Saenca '+year}
                        </Typography>
                    </Grid>
                </Grid>
            </div>
         );
    }
}
 
export default withStyles(styles)(Footer);
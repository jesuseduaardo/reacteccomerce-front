import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Sumary from '../sumary/Sumary';
import "./shoppingCart.css";
import history from "../history";

import {checkout} from "../../data";

const styles = theme => ({
    root: {
    },
    paper: {
        position: 'absolute',
        width: "85%",
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(1),
      },
    button: {
    margin: theme.spacing(1),
    width: '90%',
    color: "#fff",
    },
})


class ShoppingCart extends Component {
    state = { 
        sumary: [],
        open:false
     }

    componentDidMount(){
        this.setState({
           ...this.state,
           sumary:checkout  
        })
      }
   
     updateSumary = (oldSumary) => {
       this.setState({
         ...this.state,
         sumary:oldSumary
       });
     }

    handleOpen = () => {
        this.setState({
            ...this.state,
            open:true
        })
    };
    
    handleClose = () => {
        this.setState({
            ...this.state,
            open:false
        })
    };

    getModalStyle() {
        const top = 50;
        const left = 50;
      
        return {
          top: `${top}%`,
          left: `${left}%`,
          transform: `translate(-${top}%, -${left}%)`,
        };
      }

    handleCheckout(){
      history.push("/checkout");
      this.handleClose()
    }
    render() {
        const {classes} = this.props;
        return ( 
            <div>
                <IconButton color="default" aria-label="add to shopping cart" onClick={this.handleOpen}> 
                    <ShoppingCartIcon style={{color:"#fff"}} />
                </IconButton>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    scroll="body"
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description" 
                >
                    <DialogContent dividers={false} className="modal">
                        <Sumary dataItems={this.state.sumary} updateSumary={this.updateSumary} />
                    </DialogContent>
                    <DialogActions>
                        { this.state.sumary && this.state.sumary.length ? 
                        <Button
                            variant="contained"
                            size="large"
                            className={classes.button} 
                            color="primary" 
                            onClick={()=>this.handleCheckout()}
                        > 
                            Confirmar Orden
                        </Button>
                        : 
                        <Button
                            variant="outlined"
                            size="large" 
                            onClick={this.handleClose}
                        > 
                            Cerrar
                        </Button>
                        }
                    </DialogActions>
                </Dialog>
            </div>
         );
    }
}
 
export default withStyles(styles)(ShoppingCart);
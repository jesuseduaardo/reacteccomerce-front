import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import Divider from '@material-ui/core/Divider';
import Menu from './menu/Menu';
import {isMobile} from 'react-device-detect';

const drawerWidth = isMobile ? "100%" : 350;

const styles = theme => ({
    root: {
        display: 'flex',
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      hide: {
        display: 'none',
      },
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
      },
      drawerPaper: {
        width: drawerWidth,
      },
      drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
      },
})

class SideBarMenu extends Component {

    state = { 
        open:false
     }

    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                ...this.state,
                open:this.props.open
            })
        }
    }

    toggleDrawer = open => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        this.setState({ 
            ...this.state, 
            open : open 
        }, ()=> this.props.sidebarOpen(this.state.open) );
    }
      
    render() {
        const { classes } =  this.props;
        return ( 
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={this.state.open} 
                onClose={this.toggleDrawer(false)}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div
                className={classes.list}
                role="presentation"
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.toggleDrawer(false)}>
                            <ClearIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <Menu toggleDrawer={this.toggleDrawer()}/>
                </div>
            </Drawer>
         )
    }
}
 
export default withStyles(styles)(SideBarMenu);
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import history from '../../history';

const styles = theme => ({
      nested: {
        paddingLeft: theme.spacing(4),
      },
})

class Menu extends Component {
    state = { 
        selected:null
     }

    closeMenu(){
        this.props.toggleDrawer(false);
    }

    selectPage(page){
        history.push(page);
        this.closeMenu();
    }

    handleListItemClick = (event, index, page) => {
        event.persist();
        this.setState({
            ...this.state,
            selected:index
        }, ()=>this.selectPage(page));
    };

    render() { 
        const {classes} = this.props;
        return ( 
            <div>
                <List component="nav">
                    <ListItem 
                        button 
                        selected={this.selected === 0}
                        onClick={event => this.handleListItemClick(event, 0, "/")} 
                        >
                        <ListItemIcon>
                            <HomeIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Inicio"/>
                    </ListItem>
                    <ListItem 
                        button
                        selected={this.selected === 1}
                        onClick={event => this.handleListItemClick(event, 1, "/productos")}
                        >
                        <ListItemIcon>
                            <LocalMallIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Productos" />
                    </ListItem>
                        <List component="div" disablePadding>
                            <ListItem button className={classes.nested}>
                                <ListItemIcon><ArrowForwardIosIcon/></ListItemIcon>
                                <ListItemText primary="Categoria1" />
                            </ListItem>
                            <ListItem button className={classes.nested}>
                                <ListItemIcon><ArrowForwardIosIcon/></ListItemIcon>
                                <ListItemText primary="Categoria2" />
                            </ListItem>
                            <ListItem button className={classes.nested}>
                                <ListItemIcon><ArrowForwardIosIcon/></ListItemIcon>
                                <ListItemText primary="Categoria3" />
                            </ListItem>
                        </List>
                </List>
                <Divider />
                <List>
                    {['All mail', 'Trash', 'Spam','All mail', 'Trash', 'Spam','All mail', 'Trash', 'Spam','All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text} key={index}>
                        <ListItemIcon><HomeIcon/></ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                    ))}
                </List>
            </div>
         );
    }
}
 
export default withStyles(styles)(Menu);


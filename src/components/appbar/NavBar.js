import React, { Component } from 'react';
import { withStyles, fade } from '@material-ui/core/styles';
import {withRouter} from 'react-router';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import ShoppingCart from '../shopping-cart/ShoppingCart';
import LoginForm from '../login/LoginForm';
import SideBarMenu from '../sidebarmenu/SideBarMenu';
import "./nav-bar.css";

const styles = theme => ({
    grow: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
          display: 'block',
        },
      },
      mainTitle:{
        flexGrow: 1,
      },
      search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(3),
          width: 'auto',
        },
      },
      searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      inputRoot: {
        color: 'inherit',
      },
      inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: 200,
        },
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
})


class NavBar extends Component {
  state = { 
    anchorEl:null,
    mobileMoreAnchorEl:null,
    isMobileMenuOpen:false,
    isMenuOpen:false,
    isLogged:false,
    sidebarOpen: false,
    changeColor:false,
    position:0
  }

  componentDidMount() {
    window.addEventListener('scroll', this.listenToScroll)
  }
  
  componentWillUnmount() {
    window.removeEventListener('scroll', this.listenToScroll)
  }

  listenToScroll = () => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop
  
    const height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight
  
    const scrolled = winScroll / height
    
    this.setState({
      position: scrolled,
      changeColor : (scrolled > 0.52749)
    })
  }

  botonMenu =() => {
    return (
    <IconButton 
      edge="start" 
      className="menuButton"  
      color="inherit" 
      aria-label="menu" 
      onClick = {() => this.handleSideBarMenuOpen(true)}
      >
      <MenuIcon />
    </IconButton>
    )
  }
  
  handleProfileMenuOpen = event => {
    this.setState({
      ...this.state,
      anchorEl: event.currentTarget,
      isMenuOpen:true  

    })
  };

  handleMobileMenuOpen = event => {
    this.setState({
      ...this.state,
      mobileMoreAnchorEl: event.currentTarget,
      isMobileMenuOpen:true
    })
  };
  
  handleMobileMenuClose = () => {
    this.setState({
      ...this.state,
      mobileMoreAnchorEl: null,
      isMobileMenuOpen:false

    })
  };
  
  handleMenuClose = () => {
    this.setState({
      ...this.state,
      anchorEl: null,
      isMenuOpen:false  
    }, ()=>{ this.handleMobileMenuClose() })
  };
  
  handleSideBarMenuOpen = (open) =>{
    this.setState({
      ...this.state,
      sidebarOpen: open
    })
  }

  handlePage(pathname){
    switch (pathname) {
      case "nosotros":
        return "Nosotros";
      case "productos":
        return "Productos";
      default:
        return "Error";
    }
  }

  render() { 
    const {classes} = this.props
    const menuId = 'primary-search-account-menu';
    const handle = this.props.location.pathname;
    const match = handle.match(/^.([a-z]+)/i);
        
    const renderMenu = (
      <Menu
        anchorEl={this.state.anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={menuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={this.state.isMenuOpen}
        onClose={this.state.handleMenuClose}
      >
        <MenuItem onClick={()=>this.handleMenuClose()}>Profile</MenuItem>
        <MenuItem onClick={()=>this.handleMenuClose()}>My account</MenuItem>
      </Menu>
      );
      const mobileMenuId = 'primary-search-account-menu-mobile';
      const renderMobileMenu = (
      <Menu
        anchorEl={this.state.mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={mobileMenuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={this.state.isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        <MenuItem>
          <IconButton aria-label="show 4 new mails" color="inherit">
            <Badge badgeContent={4} color="secondary">
              <MailIcon />
            </Badge>
          </IconButton>
          <p>Messages</p>
        </MenuItem>
        <MenuItem>
          <IconButton aria-label="show 11 new notifications" color="inherit">
            <Badge badgeContent={11} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
        <MenuItem onClick={(e) => this.handleProfileMenuOpen(e)}>
          <IconButton
            aria-label="account of current user"
            aria-controls="primary-search-account-menu"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
      );
    return ( 
      <React.Fragment>
          <div className={classes.grow}>
              <AppBar 
                className={this.state.changeColor ? "app-bar background" : "app-bar background-none"}
                >
                {this.state.isLogged 
                ? //if 
                  <Toolbar>
                  {this.botonMenu()}
                  <Typography className={classes.title} variant="h6" noWrap>
                      Material-UI
                  </Typography>
                  <div className={classes.search}>
                      <div className={classes.searchIcon}>
                      <SearchIcon />
                      </div>
                      <InputBase
                      placeholder="Search…"
                      classes={{
                          root: classes.inputRoot,
                          input: classes.inputInput,
                      }}
                      inputProps={{ 'aria-label': 'search' }}
                      />
                  </div>
                  <div className={classes.grow} />
                  <div className={classes.sectionDesktop}>
                      <IconButton aria-label="show 4 new mails" color="inherit">
                      <Badge badgeContent={4} color="secondary">
                          <MailIcon />
                      </Badge>
                      </IconButton>
                      <IconButton aria-label="show 17 new notifications" color="inherit">
                      <Badge badgeContent={17} color="secondary">
                          <NotificationsIcon />
                      </Badge>
                      </IconButton>
                      <IconButton
                      edge="end"
                      aria-label="account of current user"
                      aria-controls={menuId}
                      aria-haspopup="true"
                      onClick={(e)=>this.handleProfileMenuOpen(e)}
                      color="inherit"
                      >
                      <AccountCircle />
                      </IconButton>
                  </div>
                  <div className={classes.sectionMobile}>
                      <IconButton
                      aria-label="show more"
                      aria-controls={mobileMenuId}
                      aria-haspopup="true"
                      onClick={(e)=> this.handleMobileMenuOpen(e) }
                      color="inherit"
                      >
                        <MoreIcon />
                      </IconButton>
                  </div>
                  </Toolbar>
                  : //Else
                  <Toolbar>
                    {this.botonMenu()}
                    <Typography variant="h6" className={classes.mainTitle}>
                      { typeof match !== "undefined" && match!=null && Array.isArray(match) ? 
                        this.handlePage(match[1]) : 
                        "Inicio" }
                    </Typography>
                      { typeof match !== "undefined" && match!=null && Array.isArray(match) ? 
                        match[1] === "checkout" ? "" : <ShoppingCart /> : <ShoppingCart />
                      }
                    
                    <LoginForm/>
                  </Toolbar> 
                  }
              </AppBar>
              {renderMobileMenu}
              {renderMenu}
              <SideBarMenu open={this.state.sidebarOpen} sidebarOpen={this.handleSideBarMenuOpen}/>
              </div>
      </React.Fragment>
      );
    }
}
 
export default withRouter(withStyles(styles)(NavBar));